#!/usr/bin/env python
#
# Copyright 2010 Facebook
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.

import os
import datetime
import facebook

from google.appengine.ext import webapp
from google.appengine.ext.webapp import util, template

FACEBOOK_APP_ID = '222804837758828'
FACEBOOK_APP_SECRET = '757d4bad6d37810be2a707369c3ff4ef'
FACEBOOK_CANVAS_NAME = 'mauriziogregorini'
FACEBOOK_REALTIME_VERIFY_TOKEN = datetime.datetime.now()
EXTERNAL_HREF = 'http://maurizio-gregorini.appspot.com'
ADMIN_USER_IDS = ['1267486485']

class HomeHandler(facebook.FacebookHandler):
    def initialize(self, request, response):
        """General initialization for every request"""
        super(HomeHandler, self).initialize(request, response)
        
    def init_facebook(self):
        """Sets up the request specific Facebook and User instance"""
        fb = facebook.Facebook(app_id=FACEBOOK_APP_ID,app_secret=FACEBOOK_APP_SECRET)

        # initial facebook request comes in as a POST with a signed_request
        if u'signed_request' in self.request.POST:
            fb.load_signed_request(self.request.get('signed_request'))
            # we reset the method to GET because a request from facebook with a
            # signed_request uses POST for security reasons, despite it
            # actually being a GET. in webapp causes loss of request.POST data.
            self.request.method = u'GET'
            self.set_cookie(
                'u', fb.user_cookie, datetime.timedelta(minutes=1440))
        elif 'u' in self.request.cookies:
            fb.load_signed_request(self.request.cookies.get('u'))

        self.facebook = fb
        
    def get(self):
        path = os.path.join(os.path.dirname(__file__), "templates/welcome.html")
        self.response.out.write(template.render(path, {'facebook_app_id': FACEBOOK_APP_ID}))


def main():
    util.run_wsgi_app(webapp.WSGIApplication([(r".*", HomeHandler)]))


if __name__ == "__main__":
    main()